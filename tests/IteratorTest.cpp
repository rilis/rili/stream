#include <algorithm>
#include <rili/Stream.hpp>
#include <rili/Test.hpp>
#include <string>
#include <vector>

namespace detail {
template <class InputIt, class OutputIt>
OutputIt copy(InputIt first, InputIt last, OutputIt d_first) {
    while (first != last) {
        *d_first++ = *first++;
    }
    return d_first;
}
}  // namespace detail

TEST(WritableIterator, customWriter) {
    std::vector<int> data = {1, 2, 3, 4};
    rili::stream::BufferingFifoDuplex stream;

    detail::copy(data.begin(), data.end(),
                 rili::stream::WritableCustomIterator<int>(stream, [](int i) -> rili::stream::Writer {
                     return [i](rili::stream::Writable& w) -> rili::stream::Writable& { return w << "[" << i << "]"; };
                 }));

    stream << rili::stream::flush;

    std::string result(stream.readableView().data(), stream.readableView().data() + stream.readableView().size());

    EXPECT_EQ(result, "[1][2][3][4]");
}

TEST(WritableIterator, defaultWriter) {
    std::vector<int> data = {1, 2, 3, 4};
    rili::stream::BufferingFifoDuplex stream;

    detail::copy(data.begin(), data.end(), rili::stream::WritableIterator<int>(stream));

    stream << rili::stream::flush;

    std::string result(stream.readableView().data(), stream.readableView().data() + stream.readableView().size());

    EXPECT_EQ(result, "1234");
}

TEST(ReadableIterator, defaultReader) {
    std::vector<int> data;
    rili::stream::BufferingFifoDuplex stream;
    stream << "1 2 3 4" << rili::stream::endl;

    std::copy(rili::stream::ReadableIterator<int>(stream), rili::stream::ReadableIterator<int>(),
              std::back_inserter(data));

    ASSERT_EQ(data.size(), 4u);
    EXPECT_EQ(data[0], 1);
    EXPECT_EQ(data[1], 2);
    EXPECT_EQ(data[2], 3);
    EXPECT_EQ(data[3], 4);
}

TEST(ReadableIterator, customReader) {
    std::vector<int> data;
    rili::stream::BufferingFifoDuplex stream;
    stream << "1 2 3 4" << rili::stream::endl;

    std::copy(rili::stream::ReadableCustomIterator<int>(
                  stream,
                  [](int& v) -> rili::stream::Reader {
                      return [&v](rili::stream::Readable& r) -> rili::stream::OperationResult {
                          auto result = r >> v;
                          if (result) {
                              v = 10 - v;
                          }
                          return result;
                      };
                  }),
              rili::stream::ReadableCustomIterator<int>(), std::back_inserter(data));

    ASSERT_EQ(data.size(), 4u);
    EXPECT_EQ(data[0], 9);
    EXPECT_EQ(data[1], 8);
    EXPECT_EQ(data[2], 7);
    EXPECT_EQ(data[3], 6);
}
