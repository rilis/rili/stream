#include <rili/Stream.hpp>
#include <rili/Test.hpp>
#include <string>
#include <tests/StreamMocks.hpp>

TEST(BufferingWritable, numberSuccess) {
    BufferingWritableMock cut;

    BufferingWritableMock const& ccut = cut;
    EXPECT_EQ(ccut.writableView().size(), 0u);

    EXPECT_CALL(cut, commit, [](rili::stream::view::Default& view) -> std::exception_ptr {
        EXPECT_EQ(view.size(), 1u);
        EXPECT_EQ(view.raw(), "5");
        return {};
    });

    EXPECT_CALL(cut, commit, [](rili::stream::view::Default& view) -> std::exception_ptr {
        EXPECT_EQ(view.size(), 1u);
        EXPECT_EQ(view.raw(), "5");
        view.raw().clear();
        return {};
    });

    EXPECT_TRUE(cut << 5 << rili::stream::flush);
}

TEST(BufferingWritable, numberFailure) {
    BufferingWritableMock cut;
    EXPECT_CALL(cut, commit, [](rili::stream::view::Default& view) -> std::exception_ptr {
        EXPECT_EQ(view.size(), 1u);
        EXPECT_EQ(view.raw(), "5");
        return std::make_exception_ptr(1);
    });

    auto result = cut << 5 << rili::stream::flush;
    EXPECT_FALSE(result);
    EXPECT_EQ(result.brief(), rili::stream::OperationResult::Brief::WritableEnd);
}

TEST(BufferingReadable, numberSuccess) {
    BufferingReadableMock cut;
    int val = 0;

    EXPECT_CALL(cut, fetch, [](rili::stream::view::Default& view) -> std::exception_ptr {
        view.raw() += "   ";
        return {};
    });

    EXPECT_CALL(cut, fetch, [](rili::stream::view::Default& view) -> std::exception_ptr {
        view.raw() += "5";
        return {};
    });

    EXPECT_CALL(cut, fetch, [](rili::stream::view::Default& view) -> std::exception_ptr {
        view.raw() += " ";
        return {};
    });

    EXPECT_TRUE(cut >> val);
    EXPECT_EQ(val, 5);
}

TEST(BufferingReadable, fpNumberSuccess) {
    BufferingReadableMock cut;
    float val = 0;

    EXPECT_CALL(cut, fetch, [](rili::stream::view::Default& view) -> std::exception_ptr {
        view.raw() += "   ";
        return {};
    });

    EXPECT_CALL(cut, fetch, [](rili::stream::view::Default& view) -> std::exception_ptr {
        view.raw() += "3";
        return {};
    });

    EXPECT_CALL(cut, fetch, [](rili::stream::view::Default& view) -> std::exception_ptr {
        view.raw() += ".1";
        return {};
    });

    EXPECT_CALL(cut, fetch, [](rili::stream::view::Default& view) -> std::exception_ptr {
        view.raw() += "4";
        return {};
    });

    EXPECT_CALL(cut, fetch, [](rili::stream::view::Default& view) -> std::exception_ptr {
        view.raw() += " ";
        return {};
    });

    EXPECT_TRUE(cut >> val);
    EXPECT_LT(val, 3.15);
    EXPECT_GT(val, 3.13);
}

TEST(BufferingReadable, numberFailure) {
    BufferingReadableMock cut;
    int val = 0;

    EXPECT_CALL(cut, fetch, [](rili::stream::view::Default& view) -> std::exception_ptr {
        view.raw() += "   ";
        return {};
    });

    EXPECT_CALL(cut, fetch, [](rili::stream::view::Default& view) -> std::exception_ptr {
        view.raw() += "5";
        return {};
    });

    EXPECT_CALL(cut, fetch, [](rili::stream::view::Default & /*view*/) -> std::exception_ptr {
        return std::make_exception_ptr(1);
    });

    auto result = cut >> val;
    EXPECT_FALSE(result);
    EXPECT_EQ(result.brief(), rili::stream::OperationResult::Brief::ReadableEnd);
}

TEST(BufferingReadable, nonZeroPull) {
    unsigned char var[5] = {0};
    BufferingReadableMock cut;

    EXPECT_CALL(cut, fetch, [](rili::stream::view::Default& view) -> std::exception_ptr {
        view.raw() += " ";
        return {};
    });

    EXPECT_CALL(cut, fetch, [](rili::stream::view::Default& view) -> std::exception_ptr {
        view.raw() += " ";
        return {};
    });

    EXPECT_CALL(cut, fetch, [](rili::stream::view::Default& view) -> std::exception_ptr {
        view.raw() += " ";
        return {};
    });

    EXPECT_CALL(cut, fetch, [](rili::stream::view::Default& view) -> std::exception_ptr {
        view.raw() += " ";
        return {};
    });

    EXPECT_CALL(cut, fetch, [](rili::stream::view::Default& view) -> std::exception_ptr {
        view.raw() += " ";
        return {};
    });

    EXPECT_TRUE(cut >> std::function<rili::stream::OperationResult(rili::stream::Readable&)>(
                           [&var](rili::stream::Readable& r) -> rili::stream::OperationResult {
                               r.pull(5);
                               r.read(var, 5);
                               return {};
                           }));
}

TEST(BufferingFifoDuplex, readAndWriteSuccess) {
    rili::stream::BufferingFifoDuplex cut;
    EXPECT_TRUE(cut << "somestring " << 5 << " someother " << 3.14 << rili::stream::endl);
    std::string s1, s2;
    int i1;
    float f1;

    EXPECT_TRUE(cut >> s1);
    EXPECT_TRUE(cut >> i1);
    EXPECT_TRUE(cut >> s2);
    EXPECT_TRUE(cut >> f1);

    EXPECT_EQ(s1, "somestring");
    EXPECT_EQ(s2, "someother");
    EXPECT_EQ(i1, 5);
    EXPECT_LT(f1, 3.15);
    EXPECT_GT(f1, 3.13);
}

TEST(BufferingFifoDuplex, readFromEmptyFailure) {
    rili::stream::BufferingFifoDuplex cut;
    std::string s1;

    auto result = cut >> s1;
    EXPECT_FALSE(result);
    EXPECT_EQ(result.brief(), rili::stream::OperationResult::Brief::ReadableEnd);
}
