#include <rili/Stream.hpp>
#include <rili/Test.hpp>
#include <string>
#include <tests/StreamMocks.hpp>

TYPED_TEST(Readable, numericTypeInRangeSufficientView) {
    TypeParam var;
    TypeParam expected = 5;
    ReadableMock cut;
    cut.view().raw() = "5 ";
    EXPECT_CALL(cut, consume, [](std::size_t size) { EXPECT_EQ(size, 1u); });

    EXPECT_TRUE(cut >> var);
    EXPECT_EQ(var, expected);
}

TYPED_TEST(Readable, numericTypeEmptyView) {
    TypeParam var;
    ReadableMock cut;

    EXPECT_CALL(cut, pull, [](std::size_t size) -> std::exception_ptr {
        EXPECT_EQ(size, 0ul);
        return std::make_exception_ptr(rili::stream::error::ReadableEnd());
    });

    auto result = cut >> var;
    EXPECT_FALSE(result);
    EXPECT_EQ(result.brief(), rili::stream::OperationResult::Brief::ReadableEnd);
}

TYPED_TEST(Readable, numericTypeWhitespaceOnlyView) {
    TypeParam var;
    ReadableMock cut;
    auto& view = cut.view();

    EXPECT_CALL(cut, pull, [&view](std::size_t size) -> std::exception_ptr {
        EXPECT_EQ(size, 0ul);
        view.raw() += " ";
        return {};
    });

    EXPECT_CALL(cut, pull, [&view](std::size_t size) -> std::exception_ptr {
        EXPECT_EQ(size, 0ul);
        view.raw() += " ";
        return {};
    });

    EXPECT_CALL(cut, pull, [&view](std::size_t size) -> std::exception_ptr {
        EXPECT_EQ(size, 0ul);
        return std::make_exception_ptr(rili::stream::error::ReadableEnd());
    });

    auto result = cut >> var;
    EXPECT_FALSE(result);
    EXPECT_EQ(result.brief(), rili::stream::OperationResult::Brief::ReadableEnd);
}

TYPED_TEST(Readable, numericTypeUnexpectedEndOfData) {
    TypeParam var;
    ReadableMock cut;
    auto& view = cut.view();

    EXPECT_CALL(cut, pull, [&view](std::size_t size) -> std::exception_ptr {
        EXPECT_EQ(size, 0ul);
        view.raw() += " ";
        return {};
    });

    EXPECT_CALL(cut, pull, [&view](std::size_t size) -> std::exception_ptr {
        EXPECT_EQ(size, 0ul);
        view.raw() += "1";
        return {};
    });

    EXPECT_CALL(cut, pull, [&view](std::size_t size) -> std::exception_ptr {
        EXPECT_EQ(size, 0ul);
        return std::make_exception_ptr(rili::stream::error::ReadableEnd());
    });

    auto result = cut >> var;
    EXPECT_FALSE(result);
    EXPECT_EQ(result.brief(), rili::stream::OperationResult::Brief::ReadableEnd);
}

TYPED_TEST(Readable, numericTypeOutOfRangeInView) {
    TypeParam var;
    ReadableMock cut;
    cut.view().raw() = "1234567890123456789012345678901234567890 ";

    auto result = cut >> var;
    EXPECT_FALSE(result);
    EXPECT_EQ(result.brief(), rili::stream::OperationResult::Brief::OutOfRange);
}

TYPED_TEST(Readable, numericTypeUnexpectedData) {
    TypeParam var;
    ReadableMock cut;
    auto& view = cut.view();

    EXPECT_CALL(cut, pull, [&view](std::size_t size) -> std::exception_ptr {
        EXPECT_EQ(size, 0ul);
        view.raw() += "something unexpected";
        return {};
    });

    auto result = cut >> var;
    EXPECT_FALSE(result);
    EXPECT_EQ(result.brief(), rili::stream::OperationResult::Brief::UnexpectedData);
}

TYPED_TEST(Readable, fpTypeOutOfRangeInView) {
    TypeParam var;
    ReadableMock cut;
    cut.view().raw() = "1.18973e+50000 ";

    auto result = cut >> var;
    EXPECT_FALSE(result);
    EXPECT_EQ(result.brief(), rili::stream::OperationResult::Brief::OutOfRange);
}

PARAMETERIZE_TYPED_TEST(Readable, numericTypeInRangeSufficientView,
                        short,               // NOLINT (runtime/int)
                        unsigned short,      // NOLINT (runtime/int)
                        long,                // NOLINT (runtime/int)
                        unsigned long,       // NOLINT (runtime/int)
                        long long,           // NOLINT (runtime/int)
                        unsigned long long,  // NOLINT (runtime/int)
                        int, unsigned int, float, double)

PARAMETERIZE_TYPED_TEST(Readable, numericTypeEmptyView,
                        short,               // NOLINT (runtime/int)
                        unsigned short,      // NOLINT (runtime/int)
                        long,                // NOLINT (runtime/int)
                        unsigned long,       // NOLINT (runtime/int)
                        long long,           // NOLINT (runtime/int)
                        unsigned long long,  // NOLINT (runtime/int)
                        int, unsigned int, float, double, bool)

PARAMETERIZE_TYPED_TEST(Readable, numericTypeWhitespaceOnlyView,
                        short,               // NOLINT (runtime/int)
                        unsigned short,      // NOLINT (runtime/int)
                        long,                // NOLINT (runtime/int)
                        unsigned long,       // NOLINT (runtime/int)
                        long long,           // NOLINT (runtime/int)
                        unsigned long long,  // NOLINT (runtime/int)
                        int, unsigned int, float, double, bool)

PARAMETERIZE_TYPED_TEST(Readable, numericTypeUnexpectedEndOfData,
                        short,               // NOLINT (runtime/int)
                        unsigned short,      // NOLINT (runtime/int)
                        long,                // NOLINT (runtime/int)
                        unsigned long,       // NOLINT (runtime/int)
                        long long,           // NOLINT (runtime/int)
                        unsigned long long,  // NOLINT (runtime/int)
                        int, unsigned int, float, double, bool)

PARAMETERIZE_TYPED_TEST(Readable, numericTypeUnexpectedData,
                        short,               // NOLINT (runtime/int)
                        unsigned short,      // NOLINT (runtime/int)
                        long,                // NOLINT (runtime/int)
                        unsigned long,       // NOLINT (runtime/int)
                        long long,           // NOLINT (runtime/int)
                        unsigned long long,  // NOLINT (runtime/int)
                        int, unsigned int, float, double, bool)

PARAMETERIZE_TYPED_TEST(Readable, numericTypeOutOfRangeInView,
                        short,               // NOLINT (runtime/int)
                        unsigned short,      // NOLINT (runtime/int)
                        long,                // NOLINT (runtime/int)
                        unsigned long,       // NOLINT (runtime/int)
                        long long,           // NOLINT (runtime/int)
                        unsigned long long,  // NOLINT (runtime/int)
                        int, unsigned int)

PARAMETERIZE_TYPED_TEST(Readable, fpTypeOutOfRangeInView, float, double)

TEST(Readable, boolInRangeSufficientView) {
    bool var;
    ReadableMock cut;
    cut.view().raw() = "1 ";
    EXPECT_CALL(cut, consume, [](std::size_t size) { EXPECT_EQ(size, 1u); });

    EXPECT_TRUE(cut >> var);
    EXPECT_TRUE(var);
}

TEST(Readable, boolOutOfRangeInView) {
    bool var;
    ReadableMock cut;
    cut.view().raw() = "2 ";

    auto result = cut >> var;
    EXPECT_FALSE(result);
    EXPECT_EQ(result.brief(), rili::stream::OperationResult::Brief::OutOfRange);
}

TEST(Readable, ptrInRangeSufficientView) {
    void* var;
    void* expected = reinterpret_cast<void*>(0x1234);
    ReadableMock cut;

    cut.view().raw() = "0x1234 ";
    EXPECT_CALL(cut, consume, [](std::size_t size) { EXPECT_EQ(size, 6u); });

    EXPECT_TRUE(cut >> var);
    EXPECT_EQ(var, expected);
}

TEST(Readable, ptrOutOfRangeInView) {
    void* var;
    ReadableMock cut;

    cut.view().raw() = "0x11223344556677889900 ";
    auto result = cut >> var;
    EXPECT_FALSE(result);
    EXPECT_EQ(result.brief(), rili::stream::OperationResult::Brief::OutOfRange);
}

TEST(Readable, ptrUnexpectedDataInView) {
    void* var;
    ReadableMock cut;

    cut.view().raw() = "0x ";
    auto result = cut >> var;
    EXPECT_FALSE(result);
}

TEST(Readable, ptrUnexpectedData2InView) {
    void* var;
    ReadableMock cut;

    cut.view().raw() = "0";
    auto result = cut >> var;
    EXPECT_FALSE(result);
    EXPECT_EQ(result.brief(), rili::stream::OperationResult::Brief::OutOfRange);
}

TEST(Readable, ptrUnexpectedData3InView) {
    void* var;
    ReadableMock cut;

    cut.view().raw() = "r";
    auto result = cut >> var;
    EXPECT_FALSE(result);
    EXPECT_EQ(result.brief(), rili::stream::OperationResult::Brief::UnexpectedData);
}

TEST(Readable, stringWhitespaceOnlyInView) {
    std::string var;
    ReadableMock cut;
    auto& view = cut.view();

    EXPECT_CALL(cut, pull, [&view](std::size_t size) -> std::exception_ptr {
        EXPECT_EQ(size, 0ul);
        view.raw() += "   \t\n";
        return {};
    });

    EXPECT_CALL(cut, pull, [&view](std::size_t size) -> std::exception_ptr {
        EXPECT_EQ(size, 0ul);
        view.raw() += "\r ";
        return {};
    });

    EXPECT_CALL(cut, pull, [&view](std::size_t size) -> std::exception_ptr {
        EXPECT_EQ(size, 0ul);
        return std::make_exception_ptr(rili::stream::error::ReadableEnd());
    });

    auto result = cut >> var;
    EXPECT_FALSE(result);
    EXPECT_EQ(result.brief(), rili::stream::OperationResult::Brief::ReadableEnd);
}

TEST(Readable, stringEmptyView) {
    std::string var;
    ReadableMock cut;

    EXPECT_CALL(cut, pull, [](std::size_t size) -> std::exception_ptr {
        EXPECT_EQ(size, 0ul);
        return std::make_exception_ptr(rili::stream::error::ReadableEnd());
    });

    auto result = cut >> var;
    EXPECT_FALSE(result);
    EXPECT_EQ(result.brief(), rili::stream::OperationResult::Brief::ReadableEnd);
}

TEST(Readable, stringUnexpectedEndInView) {
    std::string var;
    ReadableMock cut;
    cut.view().raw() = "somestring";

    EXPECT_CALL(cut, pull, [](std::size_t size) -> std::exception_ptr {
        EXPECT_EQ(size, 0ul);
        return std::make_exception_ptr(rili::stream::error::ReadableEnd());
    });

    auto result = cut >> var;
    EXPECT_FALSE(result);
    EXPECT_EQ(result.brief(), rili::stream::OperationResult::Brief::ReadableEnd);
}

TEST(Readable, stringSufficientView) {
    std::string var;
    ReadableMock cut;
    cut.view().raw() = "somestring ";

    EXPECT_CALL(cut, consume, [](std::size_t size) { EXPECT_EQ(size, 10u); });

    auto result = cut >> var;
    EXPECT_TRUE(result);
    EXPECT_EQ(var, "somestring");
}

TEST(Readable, charSufficientView) {
    char var;
    ReadableMock cut;
    cut.view().raw() = "c";

    EXPECT_CALL(cut, consume, [](std::size_t size) { EXPECT_EQ(size, 1u); });

    auto result = cut >> var;
    EXPECT_TRUE(result);
    EXPECT_EQ(var, 'c');
}

TEST(Readable, charEmptyView) {
    char var;
    ReadableMock cut;

    EXPECT_CALL(cut, pull, [](std::size_t size) -> std::exception_ptr {
        EXPECT_EQ(size, 1ul);
        return std::make_exception_ptr(rili::stream::error::ReadableEnd());
    });

    auto result = cut >> var;
    EXPECT_FALSE(result);
    EXPECT_EQ(result.brief(), rili::stream::OperationResult::Brief::ReadableEnd);
}

TEST(Readable, ucharSufficientView) {
    unsigned char var;
    ReadableMock cut;
    cut.view().raw() = "c";

    EXPECT_CALL(cut, consume, [](std::size_t size) { EXPECT_EQ(size, 1u); });

    auto result = cut >> var;
    EXPECT_TRUE(result);
    EXPECT_EQ(var, 'c');
}

TEST(Readable, ucharEmptyView) {
    unsigned char var;
    ReadableMock cut;

    EXPECT_CALL(cut, pull, [](std::size_t size) -> std::exception_ptr {
        EXPECT_EQ(size, 1ul);
        return std::make_exception_ptr(rili::stream::error::ReadableEnd());
    });

    auto result = cut >> var;
    EXPECT_FALSE(result);
    EXPECT_EQ(result.brief(), rili::stream::OperationResult::Brief::ReadableEnd);
}

TEST(Readable, customReader) {
    unsigned char var[5] = {0};
    ReadableMock cut;
    auto& view = cut.view();

    EXPECT_CALL(cut, pull, [&view](std::size_t size) -> std::exception_ptr {
        EXPECT_EQ(size, 5u);
        view.raw() += "qwerty";
        return {};
    });

    EXPECT_CALL(cut, consume, [](std::size_t size) { EXPECT_EQ(size, 5u); });

    cut >> std::function<rili::stream::OperationResult(rili::stream::Readable&)>(
               [&var](rili::stream::Readable& r) -> rili::stream::OperationResult {
                   r.pull(5);
                   r.read(var, 5);
                   return {};
               });
}
