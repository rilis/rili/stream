#pragma once
#include <stdexcept>

namespace rili {
namespace stream {
/**
 * @brief The OperationResult class is used to indicate status of stream operation
 */
class OperationResult {
 public:
    /**
     * @brief The Brief enum is used to check rough information about failure reason
     *
     * Values description:
     * * `OK` : no error
     * * `WritableEnd` : unexpected end of writable stream underlying object - not all data may be flushed
     * * `OutOfRange` : read operation cannot be completed because provided type cannot hold value visible in stream
     *    (overflow, to small variable, value bigger/smaller than provided type max/min etc.)
     * * `UnexpectedData`: unexpected character during stream processing like 'y' character as first in number
     * * `ReadableEnd` : unexpected end of readable stream underlying object - not all data may be correctly retrieved
     *    from stream
     */
    enum class Brief { OK, WritableEnd, OutOfRange, UnexpectedData, ReadableEnd };

    /**
     * @brief OperationResult default ctor - no error
     */
    inline OperationResult() : m_brief(Brief::OK), m_eptr() {}

    /**
     * @brief OperationResult ctor which used to indicate error during stream operation processing
     * @param brief brief error description
     * @param eptr pointer to exception with details about error
     */
    inline OperationResult(Brief brief, ::std::exception_ptr eptr) : m_brief(brief), m_eptr(eptr) {}

 public:
    /**
     * @brief operator bool indicate if operation was successful
     */
    inline operator bool() const { return m_brief == Brief::OK && !m_eptr; }

    /**
     * @brief brief used to retrieve `Brief` information about failure
     * @return brief failure reason
     */
    inline Brief brief() const { return m_brief; }

    /**
     * @brief exception used to retrieve detailed information about failure
     * @return exception pointer holding detailed failure reason
     */
    inline ::std::exception_ptr exception() const { return m_eptr; }

 private:
    Brief m_brief;
    ::std::exception_ptr m_eptr;
};

namespace error {
/**
 * @brief The Error class base stream error
 */
class Error : public ::std::exception {};

/**
 * @brief The EndOfData class base stream error indicating end of data(writable or readable)
 */
class EndOfData : public Error {};

/**
 * @brief The WritableEnd class stream error indication end of writable steam
 */
class WritableEnd : public EndOfData {
 public:
    /**
     * @brief what used to retrieve exception reason message
     * @return exception reason message
     */
    const char *what() const noexcept override;
};

/**
 * @brief The ReadableEnd class stream error indication end of readable steam
 */
class ReadableEnd : public EndOfData {
 public:
    /**
     * @brief what used to retrieve exception reason message
     * @return exception reason message
     */
    const char *what() const noexcept override;
};

/**
 * @brief The UnexpectedData class stream error indicating unexpected data in stream
 */
class UnexpectedData : public Error {
 public:
    /**
     * @brief what used to retrieve exception reason message
     * @return exception reason message
     */
    const char *what() const noexcept override;
};

/**
 * @brief The OutOfRange class steam errorr indicating value reading error because of too small range of provided
 * variable
 */
class OutOfRange : public Error {
 public:
    /**
     * @brief what used to retrieve exception reason message
     * @return exception reason message
     */
    const char *what() const noexcept override;
};

}  // namespace error
}  // namespace stream
}  // namespace rili
