#pragma once
#include <istream>
#include <ostream>
#include <rili/stream/BufferingStream.hpp>
#include <vector>

namespace rili {
namespace stream {
namespace std {
/**
 * @brief The Writable class is wrapper which use std::ostream as sink for unformated data
 */
class Writable : public BufferingWritable {
 private:
    Writable() = delete;
    Writable(Writable const&) = delete;
    Writable& operator=(Writable const&) = delete;

 public:
    /**
     * @brief Writable
     * @param other
     */
    Writable(Writable&& other) = default;

    /**
     * @brief operator=
     * @param other
     * @return
     */
    Writable& operator=(Writable&& other) = default;

    /**
     * @brief Writable is used to construct Writable which as data sink use given std::ostream
     * @param ostream stream to use
     */
    explicit inline Writable(::std::ostream& ostream) : BufferingWritable(), m_stream(&ostream) {}
    virtual ~Writable() = default;

 public:
    /**
     * @brief stream used to get underlying stream
     * @return associated stream
     */
    inline ::std::ostream& stream() { return *m_stream; }

    /**
     * @brief stream used to get underlying stream
     * @return associated stream
     */
    inline ::std::ostream const& stream() const { return *m_stream; }

 private:
    ::std::exception_ptr commit(view::Default& view) final;

 private:
    ::std::ostream* m_stream;
};

/**
 * @brief The Readable class is wrapper which use std::istream as source of unformated data
 */
class Readable : public BufferingReadable {
 private:
    Readable() = delete;
    Readable(Readable const&) = delete;
    Readable& operator=(Readable const&) = delete;

 public:
    /**
     * @brief Writable
     * @param other
     */
    Readable(Readable&& other) = default;

    /**
     * @brief operator=
     * @param other
     * @return
     */
    Readable& operator=(Readable&& other) = default;

    /**
     * @brief Readable is used to construct Readable which as data source use given std::ostream
     * @param istream stream to use
     */
    explicit inline Readable(::std::istream& istream) : BufferingReadable(), m_stream(&istream) {}
    virtual ~Readable() = default;

 public:
    /**
     * @brief stream used to get underlying stream
     * @return associated stream
     */
    inline ::std::istream& stream() { return *m_stream; }

    /**
     * @brief stream used to get underlying stream
     * @return associated stream
     */
    inline ::std::istream const& stream() const { return *m_stream; }

 private:
    ::std::exception_ptr fetch(view::Default& view) final;

 private:
    ::std::istream* m_stream;
};

/**
 * @brief The WritableStreambuf class is wrapper of std::streambuf which as unformted data sink use Writable
 *
 * Using it you can create std::ostream from Writable and pass it to legacy interfaces.
 */
class WritableStreambuf : public ::std::streambuf {
 public:
    /**
     * @brief WritableStreambuf create write only std::streambuf from Writable
     * @param writable stream which should be used as data sink
     */
    explicit inline WritableStreambuf(::rili::stream::Writable& writable) : ::std::streambuf(), m_writable(writable) {}

 private:
    int_type overflow(int_type c) override;
    int sync() override;

 private:
    ::rili::stream::Writable& m_writable;
};

/**
 * @brief The ReadableStreambuf class is wrapper of std::streambuf which as unformted data source use Readable
 *
 * Using it you can create std::istream from Readable and pass it to legacy interfaces.
 */
class ReadableStreambuf : public ::std::streambuf {
 public:
    /**
     * @brief ReadableStreambuf create read only std::streambuf from Readable
     * @param readable stream which should be used as data source
     */
    explicit inline ReadableStreambuf(::rili::stream::Readable& readable)
        : ::std::streambuf(), m_readable(readable), m_buffer() {}

 private:
    int underflow() override;

 private:
    ::rili::stream::Readable& m_readable;
    ::std::vector<char> m_buffer;
};
}  // namespace std
}  // namespace stream
}  // namespace rili
