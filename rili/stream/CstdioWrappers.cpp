#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#define IS_WINDOWS
#endif

#ifdef IS_WINDOWS
#include <fcntl.h>
#include <io.h>
#endif
#define __STDC_WANT_LIB_EXT1__ 1
#include <cstdio>
#include <mutex>
#include <rili/stream/CstdioWrappers.hpp>
#include <string>
#include <vector>

namespace rili {
namespace stream {
namespace {

FILE *fileOpen(FILE **file, char const *path, char const *mode) {
#if defined(__STDC_LIB_EXT1__) || defined(IS_WINDOWS)
    auto result = ::fopen_s(file, path, mode);
    if (result == 0) {
        return *file;
    } else {
        return nullptr;
    }
#else
    *file = ::fopen(path, mode);
    return *file;
#endif
}

class WritableConsole final : public Writable {
 public:
    WritableConsole() = delete;
    WritableConsole(WritableConsole const &) = delete;
    WritableConsole &operator=(WritableConsole const &) = delete;

 public:
#ifdef IS_WINDOWS
    explicit WritableConsole(FILE *file) : Writable(), m_proxy(file), m_mutex() { _setmode(_fileno(file), _O_BINARY); }
#else
    explicit WritableConsole(FILE *file) : Writable(), m_proxy(std::freopen(nullptr, "wb", file)), m_mutex() {}
#endif
    virtual ~WritableConsole() = default;

 public:
    void write(char const *data, ::std::size_t size) override {
        ::std::lock_guard<::std::mutex> lock(m_mutex);
        m_proxy.write(data, size);
    }
    ::std::exception_ptr flush() override {
        ::std::lock_guard<::std::mutex> lock(m_mutex);
        return m_proxy.flush();
    }
    view::Base const &writableView() const override { return m_proxy.writableView(); }

 private:
    WritableFileProxy m_proxy;
    ::std::mutex m_mutex;
};

class ReadableConsole final : public Readable {
 public:
    ReadableConsole() = delete;
    ReadableConsole(ReadableConsole const &) = delete;
    ReadableConsole &operator=(ReadableConsole const &) = delete;

 public:
#ifdef IS_WINDOWS
    explicit ReadableConsole(FILE *file) : Readable(), m_proxy(file, 1), m_mutex() {
        _setmode(_fileno(file), _O_BINARY);
    }
#else
    explicit ReadableConsole(FILE *file) : Readable(), m_proxy(std::freopen(nullptr, "rb", file), 1), m_mutex() {}
#endif
    virtual ~ReadableConsole() = default;

 public:
    void consume(::std::size_t size) override {
        ::std::lock_guard<::std::mutex> lock(m_mutex);
        m_proxy.consume(size);
    }
    view::Base const &readableView() const override { return m_proxy.readableView(); }
    ::std::exception_ptr pull(::std::size_t count) override {
        ::std::lock_guard<::std::mutex> lock(m_mutex);
        return m_proxy.pull(count);
    }

 private:
    ReadableFileProxy m_proxy;
    ::std::mutex m_mutex;
};
}  // namespace

Writable &cout() {
    static WritableConsole instance(stdout);
    return instance;
}

Writable &cerr() {
    static WritableConsole instance(stderr);
    return instance;
}

Readable &cin() {
    static ReadableConsole instance(stdin);
    return instance;
}

ReadableFile::ReadableFile(const std::string &file) : ReadableFileProxy(fileOpen(&m_file, file.c_str(), "rb"), 1024) {}

ReadableFile::ReadableFile(const std::string &file, size_t preferedChunkSize)
    : ReadableFileProxy(fileOpen(&m_file, file.c_str(), "rb"), preferedChunkSize) {}

ReadableFile::~ReadableFile() {
    if (m_file) {
        ::fclose(m_file);
    }
}

WritableFile::WritableFile(const std::string &file) : WritableFileProxy(fileOpen(&m_file, file.c_str(), "wb")) {}

WritableFile::WritableFile(const std::string &file, bool append)
    : WritableFileProxy(fileOpen(&m_file, file.c_str(), (append ? "ab" : "wb"))) {}

WritableFile::~WritableFile() {
    if (m_file) {
        flush();
        ::fclose(m_file);
    }
}

void WritableFileProxy::write(const char *data, ::std::size_t size) {
    m_view.raw().insert(m_view.raw().end(), data, data + size);
}

const view::Base &WritableFileProxy::writableView() const { return m_view; }

void ReadableFileProxy::consume(::std::size_t size) {
    m_view.raw().erase(m_view.raw().begin(),
                       ::std::next(m_view.raw().begin(),
                                   static_cast<rili::stream::view::Default::container_type::difference_type>(size)));
}

const view::Base &ReadableFileProxy::readableView() const { return m_view; }

::std::exception_ptr WritableFileProxy::flush() {
    auto count = ::fwrite(m_view.data(), sizeof(char), m_view.size(), m_file);
    ::fflush(m_file);
    if (count < m_view.size()) {
        m_view.raw().erase(
            m_view.raw().begin(),
            ::std::next(m_view.raw().begin(),
                        static_cast<rili::stream::view::Default::container_type::difference_type>(count)));
        return ::std::make_exception_ptr(error::WritableEnd());
    } else {
        m_view.raw().clear();
        return {};
    }
}
::std::exception_ptr ReadableFileProxy::pull(::std::size_t count) {
    if (count == 0) {
        count = m_preferedChunkSize;
    }
    ::std::vector<char> buf(count);
    ::std::size_t result = ::fread(buf.data(), sizeof(char), count, m_file);
    if (result == count) {
        m_view.raw().insert(m_view.raw().end(), buf.begin(), buf.end());
        return {};
    } else {
        m_view.raw().insert(m_view.raw().end(), buf.begin(),
                            ::std::next(buf.begin(), static_cast<::std::vector<char>::difference_type>(result)));
        return ::std::make_exception_ptr(error::ReadableEnd());
    }
}

}  // namespace stream
}  // namespace rili
