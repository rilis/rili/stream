#pragma once
#include <cstddef>
#include <string>

namespace rili {
namespace stream {
namespace view {
/**
 * @brief The Base class is interface for stream already avaliable data view
 *
 * @note stream converters may depend on that, processed data ends with `0x0` - even if it is not part of 'real data' at
 * least `data()[size()]` should point to `0x0` to not allow access to not aquired memory by functions like
 * `std::strtol` or
 * `std::strtod`
 */
class Base {
 private:
    Base(Base const&) = delete;
    Base& operator=(Base const&) = delete;

 protected:
    /**
     * @brief Base
     * @param other
     */
    Base(Base&& other) = default;
    /**
     * @brief operator =
     * @param other
     * @return
     */
    Base& operator=(Base&& other) = default;

 public:
    /**
     * @brief data give access to begin of continous memory space with size equal to size() + 1(for leading 0x0 val)
     * containing
     * already aquired data.
     * @return pointer to memory space
     */
    virtual char const* data() const = 0;

    /**
     * @brief size used to check how many bytes is in aquired data
     * @return size of data
     */
    virtual ::std::size_t size() const = 0;

 public:
    inline Base() = default;
    virtual ~Base();

 public:
    /**
     * @brief empty check if any already avaliable data
     * @return
     */
    inline bool empty() const { return size() == 0; }

    /**
     * @brief operator [] give access to data element
     * @param position in data
     * @return element from data
     */
    inline char operator[](::std::size_t position) const { return *(data() + position); }
};

/**
 * @brief The Default class provide basic view implementation
 */
class Default final : public Base {
 public:
    /**
     * @brief container_type type in which data is buffered
     */
    typedef ::std::string container_type;
    inline Default() = default;
    ~Default() = default;
    /**
     * @brief Default
     * @param other
     */
    Default(Default&& other) = default;
    /**
     * @brief operator =
     * @param other
     * @return
     */
    Default& operator=(Default&& other) = default;
    char const* data() const override;
    ::std::size_t size() const override;

    /**
     * @brief raw used to access buffer for modification
     * @return buffer
     */
    inline container_type& raw() { return m_buffer; }

    /**
     * @brief raw used to access buffer
     * @return buffer
     */
    inline container_type const& raw() const { return m_buffer; }

 private:
    container_type m_buffer;
};
}  // namespace view
}  // namespace stream
}  // namespace rili
