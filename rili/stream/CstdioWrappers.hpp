#pragma once
#include <rili/stream/BufferingStream.hpp>
#include <string>

namespace rili {
namespace stream {
/**
 * @brief The WritableFileProxy class use open writable FILE* to flush data but not manage it lifetime
 */
class WritableFileProxy : public Writable {
 private:
    WritableFileProxy() = delete;
    WritableFileProxy(WritableFileProxy const&) = delete;
    WritableFileProxy& operator=(WritableFileProxy const&) = delete;

 public:
    virtual ~WritableFileProxy() = default;

 public:
    /**
     * @brief WritableFileProxy check if file is not nullptr and construct stream, otherwise throw
     * std::invalid_argument
     * exception
     * @param writableFile ready to use open writable FILE*
     */
    inline explicit WritableFileProxy(FILE* writableFile) : m_file(writableFile), m_view() {
        if (!m_file) {
            throw ::std::invalid_argument("rili::stream::WritableFileProxy: writableFile is nullptr.");
        }
    }

 public:
    void write(char const* data, ::std::size_t size) override;
    ::std::exception_ptr flush() override;
    view::Base const& writableView() const override;

 protected:
    /// @cond INTERNAL
    FILE* m_file;
    view::Default m_view;
    /// @endcond INTERNAL
};

/**
 * @brief The ReadableFileProxy class use open writable FILE* to read data but not manage it lifetime
 */
class ReadableFileProxy : public Readable {
 private:
    ReadableFileProxy() = delete;
    ReadableFileProxy(ReadableFileProxy const& other) = delete;
    ReadableFileProxy& operator=(ReadableFileProxy const&) = delete;

 public:
    virtual ~ReadableFileProxy() = default;

 public:
    /**
     * @brief ReadableFileProxy check if file is not nullptr and construct stream with prefferedChunkSize = 1024,
     * otherwise throw std::invalid_argument exception.
     * @param readableFile ready to use open readable FILE*
     */
    inline explicit ReadableFileProxy(FILE* readableFile) : m_file(readableFile), m_preferedChunkSize(1024), m_view() {
        if (!m_file) {
            throw ::std::invalid_argument("rili::stream::ReadableFileProxy: writableFile is nullptr.");
        }
    }
    /**
     * @brief ReadableFileProxy check if file is not nullptr and construct stream with given prefferedChunkSize,
     * otherwise throw exception.
     * @param readableFile ready to use open readable FILE*
     * @param preferedChunkSize number of bytes which will be tried to fetch at one shot from file if other size not
     * explicitely asked for pull(std::size_t count) method. In case of unreliable/unpredictiable blocking data
     * sources
     * it's better to keep it relatively small. Otherwise read operation will block until whole chunk is avaliable even
     * if higher level Reader do not need whole chunk. For reliable data sources like normal files bigger values will
     * benefit in better preformance.
     */
    inline ReadableFileProxy(FILE* readableFile, ::std::size_t preferedChunkSize)
        : m_file(readableFile), m_preferedChunkSize((preferedChunkSize == 0 ? 1 : preferedChunkSize)), m_view() {
        if (!m_file) {
            throw ::std::invalid_argument("rili::stream::ReadableFileProxy: writableFile is nullptr.");
        }
    }

 public:
    void consume(::std::size_t size) override;
    view::Base const& readableView() const override;
    ::std::exception_ptr pull(::std::size_t count) override;

 protected:
    /// @cond INTERNAL
    FILE* m_file;
    const ::std::size_t m_preferedChunkSize;
    view::Default m_view;
    /// @endcond INTERNAL
};
/**
 * @brief The WritableFile class create stream which can be used to write data to given file
 */
class WritableFile : public WritableFileProxy {
 public:
    /**
     * @brief WritableFile try to open file from given path in "wb" mode and construct stream
     * @param file - path of file to open
     *
     */
    explicit WritableFile(::std::string const& file);

    /**
     * @brief WritableFile try to open file from given path in "wb" mode if append=false, in "ab" mode otherwise and
     * construct stream
     * @param file - path of file to open
     * @param append - if true will open file in append mode, otherwise in write mode
     *
     */
    WritableFile(::std::string const& file, bool append);

    /**
     * @brief ~WritableFile will close managed file if it's open
     */
    virtual ~WritableFile();
};

/**
 * @brief The ReadableFile class is RAII stream which is able to read data sequentially from file.
 */
class ReadableFile : public ReadableFileProxy {
 public:
    /**
     * @brief ReadableFile try open file from given path in "rb" mode and construct stream with prefferedChunkSize =
     * 1024.
     * @param file - path of file to open
     *
     */
    explicit ReadableFile(::std::string const& file);

    /**
     * @brief ReadableFile try open file from given path in "rb" mode and construct stream.
     * @param file - path of file to open
     * @param preferedChunkSize - number of bytes which will be tried to fetch at one shot from file if other size not
     * explicitely asked for pull(std::size_t count) method. In case of unreliable/unpredictiable blocking data
     * sources
     * it's better to keep it relatively small. Otherwise read operation will block until whole chunk is avaliable even
     * if higher level Reader do not need whole chun. For reliable data sources like normal files bigger values will
     * benefit in better preformance.
     *
     */
    ReadableFile(::std::string const& file, ::std::size_t preferedChunkSize);

    /**
     * @brief ~ReadableFile will close managed file if it's open
     */
    virtual ~ReadableFile();
};

/**
 * @brief cout give access to Writable attached to stdout - you can use it similar way as `std::cout`
 * @return instance of stdout stream
 */
Writable& cout();

/**
 * @brief cerr give access to Writable attached to stderr - you can use it similar way as `std::cerr`
 * @return instance of stderr stream
 */
Writable& cerr();

/**
 * @brief cin give access to Readable attached to stdin - you can use it similar way as `std::cin`
 * @return instance of stdin stream
 */
Readable& cin();

}  // namespace stream
}  // namespace rili
