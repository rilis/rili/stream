#include <rili/stream/StdWrappers.hpp>

namespace rili {
namespace stream {
namespace std {
::std::exception_ptr Writable::commit(view::Default& view) {
    m_stream->write(view.data(), static_cast<::std::streamsize>(view.size())).flush();
    if (m_stream->good()) {
        view.raw().clear();
        return {};
    } else {
        return ::std::make_exception_ptr(error::WritableEnd());
    }
}

::std::exception_ptr Readable::fetch(view::Default& view) {
    char data;
    m_stream->read(&data, 1);
    if (m_stream->good()) {
        view.raw().push_back(data);
        return {};
    } else {
        return ::std::make_exception_ptr(error::ReadableEnd());
    }
}

WritableStreambuf::int_type WritableStreambuf::overflow(WritableStreambuf::int_type c) {
    if (c != ::std::streambuf::traits_type::eof()) {
        auto tchar = ::std::streambuf::traits_type::to_char_type(c);
        m_writable.write(&tchar, 1);
    }
    return c;
}

int WritableStreambuf::sync() {
    if (!m_writable.flush()) {
        return 0;
    } else {
        return -1;
    }
}

int ReadableStreambuf::underflow() {
    if (m_readable.readableView().empty() && m_readable.pull(1)) {
        return ::std::streambuf::traits_type::eof();
    }
    m_buffer.resize(m_readable.readableView().size());
    m_readable.read(m_buffer.data(), m_readable.readableView().size());
    setg(m_buffer.data(), m_buffer.data(), m_buffer.data() + m_buffer.size());
    return ::std::streambuf::traits_type::to_int_type(m_buffer[0]);
}

}  // namespace std
}  // namespace stream
}  // namespace rili
