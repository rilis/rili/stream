#pragma once
#include <cstddef>
#include <rili/stream/Stream.hpp>
#include <rili/stream/View.hpp>

namespace rili {
namespace stream {
/**
 * @brief The BufferingWritable class is interface specialization of Writable which use buffer view (view::Default)
 */
class BufferingWritable : public Writable {
 private:
    BufferingWritable(BufferingWritable const&) = delete;
    BufferingWritable& operator=(BufferingWritable const&) = delete;

 public:
    inline BufferingWritable() = default;
    virtual ~BufferingWritable() = default;
    /**
     * @brief BufferingWritable
     * @param other
     */
    BufferingWritable(BufferingWritable&& other) = default;
    /**
     * @brief operator =
     * @param other
     * @return
     */
    BufferingWritable& operator=(BufferingWritable&& other) = default;

 public:
    void write(char const* data, ::std::size_t size) override;
    ::std::exception_ptr flush() override;
    view::Base const& writableView() const override;

 protected:
    /**
     * @brief commit used by BufferingWritable implementation to pass current buffered view to underlying sink
     * implementation which should shrink provided view. User should implement it to move data from view to sink.
     * @param view data to be moved to sink
     * @return operation error exception if occured.
     *
     * @note implementation of this method is not obligated to move all data from  view to sink. You can not mova
     * anything or only that much as you can at once. BufferingWritable will care to call it again latter if still
     * needed to push data to sink.
     */
    virtual ::std::exception_ptr commit(view::Default& view) = 0;

 private:
    view::Default m_view;
};

/**
 * @brief The BufferingReadable class is interface specialization of Readable which use buffer view (view::Default)
 */
class BufferingReadable : public Readable {
 private:
    BufferingReadable(BufferingReadable const&) = delete;
    BufferingReadable& operator=(BufferingReadable const&) = delete;

 public:
    inline BufferingReadable() = default;
    virtual ~BufferingReadable() = default;
    /**
     * @brief BufferingReadable
     * @param other
     */
    BufferingReadable(BufferingReadable&& other) = default;
    /**
     * @brief operator =
     * @param other
     * @return
     */
    BufferingReadable& operator=(BufferingReadable&& other) = default;

 public:
    void consume(::std::size_t size) override;
    view::Base const& readableView() const override;
    ::std::exception_ptr pull(::std::size_t count) override;

 protected:
    /**
     * @brief fetch used by BufferingReadable to retrieve data from sink. It should be implemented in way which insert
     * to view data avaliable in sink so far
     * @param view is view which should be modified
     * @return operation error exception if occured.
     *
     * @note implementation is not abligated to insert any data to view if no data avaliable
     */
    virtual ::std::exception_ptr fetch(view::Default& view) = 0;

 private:
    view::Default m_view;
};

/**
 * @brief The BufferingFifoDuplex class is specialization of Duplex which use single buffer both for read and write
 * operations. Data is inserted at the end of buffer and retrieved from beginning of buffer.
 *
 * It is very similar to std::stringstream
 */
class BufferingFifoDuplex final : public Duplex {
 public:
    inline BufferingFifoDuplex() = default;
    /**
     * @brief BufferingFifoDuplex
     * @param other
     * @return
     */
    inline BufferingFifoDuplex(BufferingFifoDuplex&& other) = default;

    /**
     * @brief BufferingFifoDuplex
     * @param other
     * @return
     */
    inline BufferingFifoDuplex& operator=(BufferingFifoDuplex&& other) = default;
    virtual ~BufferingFifoDuplex() = default;

 public:
    void consume(::std::size_t size) override;
    view::Base const& readableView() const override;
    view::Base const& writableView() const override;
    ::std::exception_ptr pull(::std::size_t count) override;
    void write(char const* data, ::std::size_t size) override;
    ::std::exception_ptr flush() override;

 private:
    view::Default m_view;
};
}  // namespace stream
}  // namespace rili
